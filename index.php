<?php
// Generate a random number between 1 and 100
$number = rand(1, 100);

// Initialize variables
$guess = null;
$attempts = 0;
$message = '';

// Check if a guess has been submitted
if (isset($_POST['guess'])) {
    // Retrieve the guess from the form submission
    $guess = $_POST['guess'];
    $attempts++;

    // Check if the guess is correct
    if ($guess == $number) {
        $message = "Congratulations! You guessed the correct number in $attempts attempts.";
    } elseif ($guess < $number) {
        $message = "Too low! Try again.";
    } else {
        $message = "Too high! Try again.";
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Guessing Game</title>
</head>
<body>
    <h1>Guessing Game</h1>
    <p><?php echo $message; ?></p>
    <form method="POST" action="">
        <input type="number" name="guess">
        <input type="submit" value="Submit">
    </form>
</body>
</html>
